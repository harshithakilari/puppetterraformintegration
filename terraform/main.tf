provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "example" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  key_name      = "myec2key"
  tags = {
    Name = "example-instance"
  }
  connection {
    type     = "ssh"
    user     = "ec2-user"  
    private_key = file("${path.module}/myec2key.pem") 
    host        = self.public_ip   
  }
  provisioner "remote-exec" {
  inline = [
    "sudo yum install -y puppet",
    "sudo puppet apply /etc/puppet/manifests/site.pp" ,
    "sudo puppet agent --test"
  ]
  }
}


